package com.appsocool.inetchat.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.appsocool.inetchat.AppController;
import com.appsocool.inetchat.R;
import com.appsocool.inetchat.model.ModelUserFacebook;

import java.util.List;

/**
 * Created by phitsanu on 8/10/15 AD.
 */
public class UserAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<ModelUserFacebook> userItems;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public UserAdapter(Activity activity, List<ModelUserFacebook> userItems) {
        this.activity = activity;
        this.userItems = userItems;
    }

    @Override
    public int getCount() {
        return userItems.size();
    }

    @Override
    public Object getItem(int location) {
        return userItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_item_friend, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        NetworkImageView thumbNail = (NetworkImageView) convertView.findViewById(R.id.userImage);
        TextView title = (TextView) convertView.findViewById(R.id.userName);

         // getting movie data for the row
        ModelUserFacebook m = userItems.get(position);

        // thumbnail image
        thumbNail.setImageUrl(m.getQuickbloxImage(), imageLoader);

        // title
        title.setText(m.getQuickbloxFullName());

        return convertView;
    }

}