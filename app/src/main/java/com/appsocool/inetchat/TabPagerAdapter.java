package com.appsocool.inetchat;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class TabPagerAdapter extends FragmentStatePagerAdapter {
    public TabPagerAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int i) {
		switch (i) {
			case 0:
				//Fragement for Android Tab
				return new Tab1Activity();
			case 1:
				//Fragment for Ios Tab
				return new Tab2Activity();
			case 2:
				//Fragment for Windows Tab
				return new Tab3Activity();
			case 3:
				//Fragment for Windows Tab
				return new Tab4Activity();
		}
		return null;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 4; //No of Tabs
	}


    }