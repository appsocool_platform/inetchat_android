package com.appsocool.inetchat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.appsocool.inetchat.model.ModelUserFacebook;
import com.appsocool.inetchat.quickblox.core.ChatService;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class LoginUserActivity extends Activity {

    private ProgressDialog progress;

    private CallbackManager callbackManager;
    private LoginButton loginButton;

    public ModelUserFacebook modelUserFacebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        QBSettings.getInstance().fastConfigInit("26631", "5ZWaNkbd3GPsSV6", "LWmxkpN4fb7B-Fq");

        loginFacebook();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void loginFacebook() {


        // Initialize Facebook Sdk before UI
        FacebookSdk.sdkInitialize(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_login_user);

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile, email"));

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d("Log", "fblogin onSuccess");

                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code
                                Log.v("LoginActivity", response.toString());

                                String fbUserId = object.optString("id");
                                String fbUserName = object.optString("name");
                                String fbUserEmail = object.optString("email");
                                String fbUserGender = object.optString("gender");
                                String fbUserQuickbloxUser = object.optString("id");
                                String fbUserQuickbloxPass = object.optString("id");

                                SharedPreferences myPrefs = getSharedPreferences("myuser", Context.MODE_PRIVATE);
                                SharedPreferences.Editor prefsEditor = myPrefs.edit();
                                prefsEditor.putString("fbid", fbUserId);
                                prefsEditor.putString("fbname", fbUserName);
                                prefsEditor.putString("fbemail", fbUserEmail);
                                prefsEditor.putString("fbgender", fbUserGender);
                                prefsEditor.putString("quickbloxuser", fbUserQuickbloxUser);
                                prefsEditor.putString("quickbloxpass", fbUserQuickbloxPass);
                                prefsEditor.commit();

                                getUserFacebook();

                            }
                        });
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                // App code
                Log.d("Log", "fblogin onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d("Log", "fblogin onError");
            }

        });


        //-- Check Facebook Token
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken == null) {
            Log.d("Log", "AccessToken" + "Signed Out");
        } else {
            Log.d("Log", "AccessToken" + "Signed In");
            getUserFacebook();
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void getUserFacebook() {

        progress = ProgressDialog.show(this, "Loading...",
                "please wait a moment", true);

        SharedPreferences myPrefs = this.getSharedPreferences("myuser", Context.MODE_PRIVATE);

        if(!myPrefs.equals(null)) {

            Log.d("Log", "parameters="+myPrefs);

            String fbUserId = myPrefs.getString("fbid", "fbid");
            String fbUserName = myPrefs.getString("fbname", "fbname");
            String fbUserEmail = myPrefs.getString("fbemail", "fbemail");
            String fbUserGender = myPrefs.getString("fbgender", "fbgender");
            String fbUserQuickbloxUser = myPrefs.getString("quickbloxuser", "quickbloxuser");
            String fbUserQuickbloxPass = myPrefs.getString("quickbloxpass", "quickbloxpass");

            Log.d("Log", "fbUserId=" + fbUserId);
            Log.d("Log", "fbUserName=" + fbUserName);
            Log.d("Log", "fbUserEmail=" + fbUserEmail);
            Log.d("Log", "fbUserGender=" + fbUserGender);
            Log.d("Log", "fbUserQuickbloxUser=" + fbUserQuickbloxUser);
            Log.d("Log", "fbUserQuickbloxPass=" + fbUserQuickbloxPass);

            modelUserFacebook = new ModelUserFacebook();

            modelUserFacebook.setFbId(fbUserId);
            modelUserFacebook.setFbName(fbUserName);
            modelUserFacebook.setFbEmail(fbUserEmail);
            modelUserFacebook.setFbGender(fbUserGender);
            modelUserFacebook.setFbGender(fbUserGender);
            modelUserFacebook.setQuickbloxUser(fbUserQuickbloxUser);
            modelUserFacebook.setQuickbloxPass(fbUserQuickbloxPass);

            Log.d("Log", "modelUserFacebook fbUserId=" + modelUserFacebook.getFbId());
            Log.d("Log", "modelUserFacebook fbUserName=" + modelUserFacebook.getFbName());
            Log.d("Log", "modelUserFacebook fbUserEmail=" + modelUserFacebook.getFbEmail());
            Log.d("Log", "modelUserFacebook fbUserGender=" + modelUserFacebook.getFbGender());
            Log.d("Log", "modelUserFacebook fbUserQuickbloxUser=" + modelUserFacebook.getQuickbloxUser());
            Log.d("Log", "modelUserFacebook fbUserQuickbloxPass=" + modelUserFacebook.getQuickbloxPass());

            quickbloxCreateSession();
        }
    }

    public void quickbloxCreateSession(){
        QBAuth.createSession(new QBEntityCallbackImpl<QBSession>() {
            @Override
            public void onSuccess(QBSession session, Bundle params) {
                // success
                Log.d("Log", "quickbloxCreateSession onSuccess");
                quickbloxSignUp();
            }

            @Override
            public void onError(List<String> errors) {
                // errors
                Log.d("Log", "quickbloxCreateSession onError");
            }
        });
    }
    public void quickbloxSignUp(){
        final QBUser user = new QBUser(modelUserFacebook.getQuickbloxUser(), modelUserFacebook.getQuickbloxPass());
        QBUsers.signUp(user, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle args) {
                // success
                Log.d("Log", "signUp onSuccess=" + user);
                if (!user.equals(null)) {
                    quickbloxSignIn();
                }
            }

            @Override
            public void onError(List<String> errors) {
                // error
                Log.d("Log", "signUp onError=" + user);

                if (!user.equals(null)) {
                    quickbloxSignIn();
                }
            }
        });
    }
    public void quickbloxSignIn(){
        // initialize Chat service
        // Login to REST API
        //
        final QBUser user = new QBUser();
        user.setLogin(modelUserFacebook.getQuickbloxUser());
        user.setPassword(modelUserFacebook.getQuickbloxPass());

        ChatService.initIfNeed(this);

        ChatService.getInstance().login(user, new QBEntityCallbackImpl() {

            @Override
            public void onSuccess() {
                progress.dismiss();

                // Go to Dialogs screen
                //
                Log.d("Log", "quickbloxQuickBlox onSuccess="+user);

                Intent intent = new Intent(com.appsocool.inetchat.LoginUserActivity.this, MainActivity.class);
                startActivity(intent);
                //finish();
            }

            @Override
            public void onError(List errors) {
                progress.dismiss();

                Log.d("Log", "quickbloxQuickBlox onError="+user);

                Intent intent = new Intent(com.appsocool.inetchat.LoginUserActivity.this, MainActivity.class);
                startActivity(intent);
                //finish();


                //AlertDialog.Builder dialog = new AlertDialog.Builder(SplashActivity.this);
                //dialog.setMessage("chat login errors: " + errors).create().show();
            }
        });
    }

}
