package com.appsocool.inetchat.model;

/**
 * Created by phitsanu on 8/8/15 AD.
 */
public class ModelUserFacebook {

    private String fbId;
    private String fbName;
    private String fbEmail;
    private String fbGender;

    private String quickbloxId;
    private String quickbloxFullName;
    private String quickbloxEmail;
    private String quickbloxUser;
    private String quickbloxPass;
    private String quickbloxImage;

    public void ModelUserFacebook(){

    }

    public void ModelUserFacebook (String fbId, String fbName, String fbEmail){
        this.fbId = fbId;
        this.fbName = fbName;
        this.fbEmail = fbEmail;
    }

    public void ModelUserFacebook (String quickbloxId, String quickbloxFullName, String quickbloxEmail, String quickbloxUser){
        this.quickbloxId = quickbloxId;
        this.quickbloxFullName = quickbloxFullName;
        this.quickbloxEmail = quickbloxEmail;
        this.quickbloxUser = quickbloxUser;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getFbName() {
        return fbName;
    }

    public void setFbName(String fbName) {
        this.fbName = fbName;
    }

    public String getFbEmail() {
        return fbEmail;
    }

    public void setFbEmail(String fbEmail) {
        this.fbEmail = fbEmail;
    }

    public String getFbGender() {
        return fbGender;
    }

    public void setFbGender(String fbGender) {
        this.fbGender = fbGender;
    }

    public String getQuickbloxId() {
        return quickbloxId;
    }

    public void setQuickbloxId(String quickbloxId) {
        this.quickbloxId = quickbloxId;
    }

    public String getQuickbloxEmail() {
        return quickbloxEmail;
    }

    public void setQuickbloxEmail(String quickbloxEmail) {
        this.quickbloxEmail = quickbloxEmail;
    }

    public String getQuickbloxFullName() {
        return quickbloxFullName;
    }

    public void setQuickbloxFullName(String quickbloxFullName) {
        this.quickbloxFullName = quickbloxFullName;
    }

    public String getQuickbloxUser() {
        return quickbloxUser;
    }

    public void setQuickbloxUser(String quickbloxUser) {
        this.quickbloxUser = quickbloxUser;
    }

    public String getQuickbloxPass() {
        return quickbloxPass;
    }

    public void setQuickbloxPass(String quickbloxPass) {
        this.quickbloxPass = quickbloxPass;
    }

    public String getQuickbloxImage() {
        return quickbloxImage;
    }

    public void setQuickbloxImage(String quickbloxImage) {
        this.quickbloxImage = quickbloxImage;
    }
}
