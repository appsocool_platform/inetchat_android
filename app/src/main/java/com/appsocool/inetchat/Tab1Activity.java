package com.appsocool.inetchat;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.appsocool.inetchat.Adapter.UserAdapter;
import com.appsocool.inetchat.model.ModelUserFacebook;

import java.util.ArrayList;
import java.util.List;

public class Tab1Activity extends Fragment {


    private static final String TAG = MainActivity.class.getSimpleName();

    // Movies json url
    private static final String url = "http://api.androidhive.info/json/movies.json";
    private ProgressDialog pDialog;
    private List<ModelUserFacebook> userList = new ArrayList<ModelUserFacebook>();
    private ListView listView;
    private UserAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_tab1, container, false);
        //listView = (ListView) v.findViewById(R.id.list);
        //getData();
        return v;
    }

    public void getData(){

        //adapter = new UserAdapter(getActivity(), userList);
        //listView.setAdapter(adapter);

        /*
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object listItem = listView.getItemAtPosition(position);
                TextView v = (TextView) view.findViewById(R.id.userName);
                Toast.makeText(getActivity(), "selected Item Name is " + v.getText(), Toast.LENGTH_LONG).show();
            }
        });
        */

        /*
        pDialog = new ProgressDialog(getActivity());
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();

        // changing action bar color
        //getActionBar().setBackgroundDrawable(
        //        new ColorDrawable(Color.parseColor("#1b1b1b")));

        // Creating volley request obj
        JsonArrayRequest movieReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        hidePDialog();


                        // Parsing json
//                        for (int i = 0; i < response.length(); i++) {
//                            try {
//
//                                JSONObject obj = response.getJSONObject(i);
//
//                                Log.d("Data", "obj title="+obj.getString("title"));
//
//                                ModelUserFacebook movie = new ModelUserFacebook();
//                                movie.setQuickbloxFullName(obj.getString("title"));
//                                movie.setQuickbloxImage(obj.getString("image"));
//
//                                // adding movie to movies array
//                                userList.add(movie);
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//
//                        }


                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        //adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(movieReq);
        */
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

}

