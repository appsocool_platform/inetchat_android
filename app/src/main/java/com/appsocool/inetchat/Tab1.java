package com.appsocool.inetchat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Edwin on 15/02/2015.
 */
public class Tab1 extends Fragment {

    private String[] mNames = {"Fabian", "Carlos", "Alex", "Andrea", "Karla",
            "Freddy", "Lazaro", "Hector", "Carolina", "Edwin", "Jhon",
            "Edelmira", "Andres"};

    private String[] mAnimals = {"Perro", "Gato", "Oveja", "Elefante", "Pez",
            "Nicuro", "Bocachico", "Chucha", "Curie", "Raton", "Aguila",
            "Leon", "Jirafa"};

    int[] flags = new int[]{
            R.drawable.img_profile,
            R.drawable.img_profile,
            R.drawable.img_profile,
            R.drawable.img_profile,
            R.drawable.img_profile,
            R.drawable.img_profile,
            R.drawable.img_profile,
            R.drawable.img_profile,
            R.drawable.img_profile,
            R.drawable.img_profile
    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //View v =inflater.inflate(R.layout.tab_1,container,false);
        //return v;

        //
//        View ios = inflater.inflate(R.layout.activity_tab2, container, false);
//        ((TextView)ios.findViewById(R.id.textView)).setText("Tab-2");
//        return ios;

        List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

        for (int i = 0; i < 10; i++) {
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("txt", "App Name : " + mNames[i]);
            hm.put("cur", "creator : " + mAnimals[i]);
            hm.put("flag", Integer.toString(flags[i]));
            aList.add(hm);
        }
        String[] from = {"flag", "txt", "cur"};

        int[] to = {R.id.flag, R.id.txt, R.id.cur};

        View v = inflater.inflate(R.layout.activity_tab2, container, false);
        ListView list = (ListView) v.findViewById(R.id.listView);
        SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), aList, R.layout.list_item_chat, from, to);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.d("Log", "Click position=" + position);

                Intent intent = new Intent(getActivity(), ChatActivity.class);
                startActivity(intent);

            }
        });
        return v;
    }
}
